import { NgModule, Component } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { IndexComponent } from './index/index.component';
import {AboutComponent} from "./shared/about/about.component";

const routes: Routes = [
  { path: 'inicio', component: IndexComponent},
  { path: 'about', component: AboutComponent},
  { path: 'index', component: IndexComponent},
  { path: 'login', component: LoginComponent },
  { path:'', redirectTo:'inicio',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {onSameUrlNavigation: 'reload'}
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
